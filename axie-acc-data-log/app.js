const axios = require('axios');
var AWS = require('aws-sdk');
var moment = require('moment');

exports.lambdaHandler = async (event, context) => {

    var accList = [
        "7d17eaed2f9e6c6b6f82c1ceb44337445ebea8f5",
        "4bf977ffa6bc6dfc332cdf9a6b2e05a6a2f1a36a",
        "796b72e35940687d244f996cf7bf231cb66c9c2c",
        "b830a4374cb53f98d6b91daaf1e4157145c86132",
        "a5d3e934a311af88dde6fd892e22b744fa25a1f6",
        "0a76450e3d7513019d4250a3bc6098ec3236953f",
        "16b96ff18dc3231b4ac34d797cea3112b219f7ea",
        "4573014322f7b9720ad51e3f5e25d837ba81490b",
        "68042c7697154265fa8476cec04af7d69c214fc6",
        "e2298277cd0748553025cf85e2c816b4c88ef884",
        "bb9809922057ed6fb2631bc4d01c5f88d0f1eeec"
    ];

    var dynamoDB = new AWS.DynamoDB();

    for(var acc of accList){
        const res = await axios.get('https://api.axie.management/v1/overview/0x' + acc, {
            headers: {
                'Origin': 'https://scholar.axie.management'
            }
        });
        console.log(res);
        await dynamoDB.putItem({
            "TableName": "axieAccount",
            "Item": {
                "roninAddress": { "S": acc },
                "createdAt": { "N": moment().unix().toString() },
                "dailyData": {
                    "M": {
                        "slp": {
                            "N": res.data.slp.total.toString()
                        }
                    }
                },
            }
        }, function (err) {
            if (err) {
                console.log(err);
            }
        }).promise();
    }
    console.log("items inserted with success");
};
